package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MarsRoverTest {

    @Test
    void should_change_location_y_to_1_when_command_is_move_given_location_0_0_North() {
        Location location = new Location(0,0,Direction.North);
        MarsRover marsRover = new MarsRover(location);
        marsRover.changeLocation(Command.move);
        Location newLocation = marsRover.getLocation();
        Assertions.assertEquals(1, newLocation.getY());
    }
    @Test
    void should_change_location_North_to_West_when_command_is_turnLeft_given_location_0_0_North() {
        Location location = new Location(0,0,Direction.North);
        MarsRover marsRover = new MarsRover(location);
        marsRover.changeLocation(Command.turnLeft);
        Location newLocation = marsRover.getLocation();
        Assertions.assertEquals(Direction.West, newLocation.getDirection());
    }
    @Test
    void should_change_location_North_to_East_when_command_is_turnRight_given_location_0_0_North() {
        Location location = new Location(0,0,Direction.North);
        MarsRover marsRover = new MarsRover(location);
        marsRover.changeLocation(Command.turnRight);
        Location newLocation = marsRover.getLocation();
        Assertions.assertEquals(Direction.East, newLocation.getDirection());
    }
    @Test
    void should_change_location_x_to_1_when_command_is_move_given_location_0_0_East() {
        Location location = new Location(0,0,Direction.East);
        MarsRover marsRover = new MarsRover(location);
        marsRover.changeLocation(Command.move);
        Location newLocation = marsRover.getLocation();
        Assertions.assertEquals(1, newLocation.getX());
    }
    @Test
    void should_change_location_East_to_North_when_command_is_turnLeft_given_location_0_0_East() {
        Location location = new Location(0,0,Direction.East);
        MarsRover marsRover = new MarsRover(location);
        marsRover.changeLocation(Command.turnLeft);
        Location newLocation = marsRover.getLocation();
        Assertions.assertEquals(Direction.North, newLocation.getDirection());
    }
    @Test
    void should_change_location_East_to_South_when_command_is_turnRight_given_location_0_0_East() {
        Location location = new Location(0,0,Direction.East);
        MarsRover marsRover = new MarsRover(location);
        marsRover.changeLocation(Command.turnRight);
        Location newLocation = marsRover.getLocation();
        Assertions.assertEquals(Direction.South, newLocation.getDirection());
    }
    @Test
    void should_change_location_Y_to_Minus1_when_command_is_move_given_location_0_0_South() {
        Location location = new Location(0,0,Direction.South);
        MarsRover marsRover = new MarsRover(location);
        marsRover.changeLocation(Command.move);
        Location newLocation = marsRover.getLocation();
        Assertions.assertEquals(-1, newLocation.getY());
    }
    @Test
    void should_change_location_South_to_East_when_command_is_turnLeft_given_location_0_0_South() {
        Location location = new Location(0,0,Direction.South);
        MarsRover marsRover = new MarsRover(location);
        marsRover.changeLocation(Command.turnLeft);
        Location newLocation = marsRover.getLocation();
        Assertions.assertEquals(Direction.East, newLocation.getDirection());
    }
    @Test
    void should_change_location_South_to_West_when_command_is_turnRight_given_location_0_0_South() {
        Location location = new Location(0,0,Direction.South);
        MarsRover marsRover = new MarsRover(location);
        marsRover.changeLocation(Command.turnRight);
        Location newLocation = marsRover.getLocation();
        Assertions.assertEquals(Direction.West, newLocation.getDirection());
    }
    @Test
    void should_change_location_X_to_Minus1_when_command_is_move_given_location_0_0_West() {
        Location location = new Location(0,0,Direction.West);
        MarsRover marsRover = new MarsRover(location);
        marsRover.changeLocation(Command.move);
        Location newLocation = marsRover.getLocation();
        Assertions.assertEquals(-1, newLocation.getX());
    }
    @Test
    void should_change_location_West_to_South_when_command_is_turnLeft_given_location_0_0_West() {
        Location location = new Location(0,0,Direction.West);
        MarsRover marsRover = new MarsRover(location);
        marsRover.changeLocation(Command.turnLeft);
        Location newLocation = marsRover.getLocation();
        Assertions.assertEquals(Direction.South, newLocation.getDirection());
    }
    @Test
    void should_change_location_West_to_North_when_command_is_turnRight_given_location_0_0_West() {
        Location location = new Location(0,0,Direction.West);
        MarsRover marsRover = new MarsRover(location);
        marsRover.changeLocation(Command.turnRight);
        Location newLocation = marsRover.getLocation();
        Assertions.assertEquals(Direction.North, newLocation.getDirection());
    }
    @Test
    void should_change_location_to_1_1_North_when_command_is_MRML_given_location_0_0_North(){
        Location location = new Location(0,0,Direction.North);
        MarsRover marsRover = new MarsRover(location);
        marsRover.changeLocation(Command.MRML);
        Location newLocation = marsRover.getLocation();
        Assertions.assertEquals(Direction.North, newLocation.getDirection());
        Assertions.assertEquals(1, newLocation.getX());
        Assertions.assertEquals(1, newLocation.getY());
    }
}
