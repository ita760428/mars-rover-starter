package com.afs.tdd;

public class Location {
    private int X;
    private int Y;
    private Direction direction ;

    public Location(int x, int y, Direction direction) {
        X = x;
        Y = y;
        this.direction = direction;
    }

    public int getY() {
        return Y;
    }

    public void setY(int y) {
        Y = y;
    }

    public Direction getDirection() {
        return direction;
    }

    public int getX() {
        return X;
    }

    public void setX(int x) {
        X = x;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }
}
