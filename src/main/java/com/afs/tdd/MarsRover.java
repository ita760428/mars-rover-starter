package com.afs.tdd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MarsRover {
    private Location location;
    public MarsRover(Location location) {
        this.location = location;
    }

    public void changeLocation(Command command) {
        if(command.equals(Command.move)){
            move();
        }
        if(command.equals(Command.turnLeft)){
            turnLeft();
        }
        if(command.equals(Command.turnRight)){
            turnRight();
        }
        if(command.equals(Command.MRML)){
            batchMovement(command);
        }
    }

    public void move(){
        Direction direction = getLocation().getDirection();
        if(direction.equals(Direction.North)){
            this.location.setY(this.location.getY()+1);
        }
        if(direction.equals(Direction.East)){
            this.location.setX(this.location.getX()+1);
        }
        if(direction.equals(Direction.South)){
            this.location.setY(this.location.getY()-1);
        }
        if(direction.equals(Direction.West)){
            this.location.setX(this.location.getX()-1);
        }
    }

    public void turnLeft(){
        Direction direction = getLocation().getDirection();
        if(direction.equals(Direction.North)){
            this.location.setDirection(Direction.West);
        }
        if(direction.equals(Direction.East)){
            this.location.setDirection(Direction.North);
        }
        if(direction.equals(Direction.South)){
            this.location.setDirection(Direction.East);
        }
        if(direction.equals(Direction.West)){
            this.location.setDirection(Direction.South);
        }
    }
    public  void  turnRight(){
        Direction direction = getLocation().getDirection();
        if(direction.equals(Direction.North)){
            this.location.setDirection(Direction.East);
        }
        if(direction.equals(Direction.East)){
            this.location.setDirection(Direction.South);
        }
        if(direction.equals(Direction.South)){
            this.location.setDirection(Direction.West);
        }
        if(direction.equals(Direction.West)){
            this.location.setDirection(Direction.North);
        }
    }
    public void batchMovement(Command command){
        String commands = command.toString();
        String[] commandSplit = commands.split("");
        List<String> commandList = Arrays.asList(commandSplit);
        commandList.stream().forEach( item -> {
            if(item.equals("M")){
                move();
            }
            if(item.equals("R")){
                turnRight();
            }
            if (item.equals("L")){
                turnLeft();
            }
        });
    }
    public Location getLocation() {
        return location;
    }
}
